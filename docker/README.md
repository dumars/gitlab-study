# gitlab

取得 gitlab-ce 的 docker image，並且使用 docker-compose 啟動，因為在 windows 環境中其 volumes 設定會發生錯誤，所以 log 及 data 這兩個設定被註解掉，如果你是 linux 或 macos 則可取消註解。

```sh
# 下載 gitlab-ce image
docker pull gitlab/gitlab-ce:latest

# 進入專案下的 docker 資料夾，使用 docker-compose 啟動 gitlab container
cd ${your_project_path}/docker
docker-compose up -d

# 初始化需要一段時間，可以打開 log 檢視初始化紀錄
docker logs -f gitlab
```

啟動完成之後可以打開瀏覽器輸入網址 [http://localhost](http://localhost)，初次連線需要設定密碼，完成密碼設定之後會被轉到登入畫面，預設帳號為 root，再使用剛剛設定的密碼就可以正常登入。

## PostgreSQL

打開 gitlab 自帶 PostgreSQL 資料庫的 TCP/IP 監聽，這樣就從外部連線進此資料庫。

使用文字編輯工具打開 `${your_project_path}/docker/config/gitlab.rb` 加入以下設定。

```yml
postgresql['listen_address'] = '*'
postgresql['port'] = 5432
postgresql['md5_auth_cidr_addresses'] = ['0.0.0.0/0']
postgresql['trust_auth_cidr_addresses'] = ["127.0.0.1/32"]
postgresql['sql_user'] = "gitlab"
postgresql['sql_user_password'] = Digest::MD5.hexdigest "gitlab" << postgresql['sql_user']
```

```yml
postgresql['ssl'] = 'off'
```

```yml
gitlab_rails['db_host'] = '127.0.0.1'
gitlab_rails['db_port'] = 5432
gitlab_rails['db_username'] = "gitlab"
gitlab_rails['db_password'] = "gitlab"
```

最後執行指令

```sh
# 進入容器
docker exec -it gitlab /bin/bash

# 執行 reconfigure
gitlab-ctl reconfigure

# 發生錯誤請執行
gitlab-ctl restart postgresql

# postgres superuser
gitlab-psql -d gitlabhq_production

# create database, gitlab 使用的 database 為 gitlabhq_production
create database dumars owner gitlab;

# 列出所有 database，檢查 dumars database 是否存在
\l

# 離開 psql 操作
\q
```

以上完成之後就可以使用 client 工具檢查是否可以連線進入資料庫。

- ip: localhost (or 127.0.0.1)
- port: 5432
- username: gitlab
- password: gitlab
- database: dumars (or gitlabhq_production)

## Prometheus

prometheus 以 gitlab-prometheus 的用戶身份執行並運行於 `http://localhost:9090`。

prometheus 設定檔為 `/var/opt/gitlab/prometheus/prometheus.yml`

```sh
# 可以將其複製出來參考
docker cp gitlab:/var/opt/gitlab/prometheus/prometheus.yml c:/
```

```sh
# 在 docker container 內執行以下操作
cd /var/opt/gitlab/prometheus/
vim prometheus.yml
```

在 `scrape_configs:` 之下加入以下設定，請注意 IP 的值(targets 設定的下一行)，因為 docker 在 windows 的網路設定限制下，沒辦法使用 host mode，所以如果設成 localhost 是指 container 自己，會連不到在 container 之外的 spring boot application。

詳細說明請參考 [https://docs.docker.com/network/host/](https://docs.docker.com/network/host/)

```yml
- job_name: "gitlab-ext"
  metrics_path: ""/actuator/prometheus"
  static_configs:
    - targets:
      - 192.168.0.100:9999
```

修改完記得儲存，然後執行

```sh
gitlab-ctl reconfigure
```

接下來啟動專案資料夾之下的 gitlab-ext

```sh
mvn spring-boot:run
```

要檢查是否設定正確，也就是要檢查 prometheus 是否能正確地連到 gitlab-ext 那就要打開 prometheus 的對外連線。

修改 `gitlab.rb`

```sh
# 加入以下其中一種設定
prometheus['listen_address'] = 'localhost:9090'
# or
prometheus['listen_address'] = ':9090'
# or
prometheus['listen_address'] = '0.0.0.0:9090'
```

修改完成之後記得要執行 `gitlab-ctl reconfigure`。

打開瀏覽器，輸入 [http://localhost:9090](http://localhost:9090)，就可以看到 prometheus 的畫面，選擇 Status -> Targets 檢查 gitlab-ext 是否存在並且連線是否正常。

## grafana

預設並沒有打開 grafana，所以要去將其打開。

使用 Administrator 角色進入 gitlab 畫面，`Admin Area -> Settings -> Metrics and Profiling` 展開 Metrics - Grafana 並打勾 Enable access to Grafana。

這樣就會多出 `Monitoring -> Metrics Dashboard` 的選項，並可以瀏覽內建的各種 gitlab dashboard。

## Redis

將預設的 redis 開放給我們的應用程式使用，所以需修改 `gitlab.rb`。

```sh
# bind ip 視需要調整 127.0.0.1 or 0.0.0.0 or 192.168.0.100 之類
redis['port'] = 6379
redis['bind'] = '0.0.0.0'
```

修改完一樣要執行

```sh
gitlab-ctl reconfigure
```

先在 container 內測試 `redis-cli ping` 有收到 PONG 就表示成功。或是安裝 redis client 工具，連線後可以看到 db0 中有 gitlab 資料。

因為資料裡面有 session 資料，因此正式使用時要限制 bind ip 及加上密碼，避免安全上的疑慮。