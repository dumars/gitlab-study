package tw.idv.dumars.gitlab.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import tw.idv.dumars.gitlab.domain.Issue;
import tw.idv.dumars.gitlab.domain.Project;
import tw.idv.dumars.gitlab.domain.enumeration.IssueLabels;
import tw.idv.dumars.gitlab.domain.enumeration.IssueScope;
import tw.idv.dumars.gitlab.domain.enumeration.IssueState;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Controller
public class SigningProcessController {

    @Value("${gitlab.api-prefix}")
    private String url;
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/signing-process")
    public String chooseProjectAndBranch(Model model) {
        Project[] projects = restTemplate.getForObject(url + "/projects", Project[].class);
        model.addAttribute("projects", projects);
        return "signing-process/request/projects";
    }

    @PostMapping("/projects/{projectId}/supervisor/{supervisorId}/commit/{commitId}")
    @ResponseBody
    public Issue createSigningIssue(@PathVariable int projectId,
                                    @PathVariable int supervisorId,
                                    @PathVariable String commitId,
                                    Principal principal) {
        Issue issue = new Issue();
        // the request data of project id is setting at id field.
        issue.setId(projectId);
        issue.setTitle(String.format("Create a new signing process request by %s", principal.getName()));
        issue.setDescription(String.format("Create by gitlab-ext system. The commit id is [%s].", commitId));
        issue.setLabels(new String[]{IssueLabels.signing_process.name()});
        issue.setAssigneeIds(new Integer[]{supervisorId});
        Issue result = restTemplate.postForObject(String.format("%s/projects/%d/issues", url, projectId), issue, Issue.class);
        log.debug("Receive create issue response: {}", result);
        return result;
    }

    @GetMapping("/projects/{projectId}/issues")
    public String findSigningIssues(@PathVariable int projectId, IssueState state, Model model) {
        StringBuilder searchUrl = new StringBuilder();
        searchUrl.append(url)
                .append("/projects/").append(projectId)
                .append("/issues?labels=").append(IssueLabels.signing_process)
                .append("&scope=").append(IssueScope.assigned_to_me)
                .append("&state=").append(Objects.requireNonNullElse(state, IssueState.opened));
        Issue[] issues = restTemplate.getForObject(searchUrl.toString(), Issue[].class);
        model.addAttribute("issues", issues);
        return "issue/opened-issues";
    }

    @PutMapping("/projects/{projectId}/issues/{id}-{iid}")
    @ResponseBody
    public Issue updateSigningIssueState(@PathVariable int projectId,
                                         @PathVariable int id,
                                         @PathVariable int iid,
                                         IssueLabels label,
                                         Principal principal) {
        if (IssueLabels.approve.equals(label) || IssueLabels.reject.equals(label)) {
            // Add new note
            Map<String, String> map = new HashMap<>();
            map.put("body", "User " + principal.getName() + " " + label.name() + " this request.");
            log.debug(map.get("body"));

            restTemplate.postForObject(String.format("%s/projects/%d/issues/%d/notes", url, projectId, iid), map, String.class);

            // Close issue
            StringBuilder builder = new StringBuilder();
            builder.append(url)
                    .append("/projects/").append(projectId)
                    .append("/issues/").append(iid)
                    .append("?id=").append(id)
                    .append("&iid=").append(iid)
                    .append("&state_event=close")
                    .append("&labels=").append(IssueLabels.signing_process).append(",").append(label);

            ResponseEntity<Issue> response = restTemplate.exchange(builder.toString(), HttpMethod.PUT, null, Issue.class);
            return response.getBody();
        } else {
            throw new IllegalArgumentException("Can't found approve or reject keyword.");
        }
    }
}
