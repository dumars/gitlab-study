package tw.idv.dumars.gitlab.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;
import tw.idv.dumars.gitlab.domain.Branch;
import tw.idv.dumars.gitlab.domain.Commit;

@Slf4j
@Controller
public class BranchController {

	@Value("${gitlab.api-prefix}")
	private String url;
	@Autowired
	private RestTemplate restTemplate;

	@GetMapping(value = "/projects/{projectId}/branches", produces = MediaType.TEXT_HTML_VALUE)
	public String showBranchList(@PathVariable int projectId, Model model) {
		Branch[] branches = restTemplate.getForObject(
				String.format("%s/projects/%d/repository/branches", url, projectId), Branch[].class);
		model.addAttribute("branches", branches);
		model.addAttribute("projectId", projectId);
		return "branch/branches";
	}

	@GetMapping("/projects/{projectId}/branches/{branchName}")
	public String showCommitsByProjectAndBranch(@PathVariable int projectId, @PathVariable String branchName, Model model) {
		Commit[] commits = restTemplate.getForObject(
				String.format("%s/projects/%d/repository/commits?branch=%s", url, projectId, branchName), Commit[].class);
		model.addAttribute("commits", commits);
		model.addAttribute("projectId", projectId);
		model.addAttribute("branchName", branchName);
		return "branch/commits";
	}

	@GetMapping(value = "/projects/{projectId}/branches-component", produces = MediaType.TEXT_HTML_VALUE)
	public String showBranchComponent(@PathVariable int projectId, Model model) {
		Branch[] branches = restTemplate.getForObject(
				String.format("%s/projects/%d/repository/branches", url, projectId), Branch[].class);
		model.addAttribute("branches", branches);
		return "branch/branches-component";
	}
}
