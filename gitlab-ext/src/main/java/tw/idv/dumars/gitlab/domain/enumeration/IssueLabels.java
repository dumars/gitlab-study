package tw.idv.dumars.gitlab.domain.enumeration;

public enum IssueLabels {
    signing_process, approve, reject;
}
