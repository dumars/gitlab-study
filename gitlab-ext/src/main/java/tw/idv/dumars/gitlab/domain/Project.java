package tw.idv.dumars.gitlab.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Project {

	private Integer id;
	private String description;
	@JsonProperty("default_branch")
	private String defaultBranch;
	@JsonProperty("ssh_url_to_repo")
	private String sshUrlToRepo;
	@JsonProperty("http_url_to_repo")
	private String httpUrlToRepo;
	@JsonProperty("web_url")
	private String webUrl;
	@JsonProperty("readme_url")
	private String readmeUrl;
	@JsonProperty("tag_list")
	private String[] tagList;
	@JsonProperty("name")
	private String name;
	@JsonProperty("name_with_namespace")
	private String nameWithNamespace;
	@JsonProperty("path")
	private String path;
	@JsonProperty("path_with_namespace")
	private String pathWithNamespace;
	@JsonProperty("created_at")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	private LocalDateTime createdAt;
	@JsonProperty("last_activity_at")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
	private LocalDateTime lastActivityAt;
	@JsonProperty("fork_count")
	private Integer forkCount;
	@JsonProperty("avatar_url")
	private String avatarUrl;
	@JsonProperty("star_count")
	private Integer starCount;
}
