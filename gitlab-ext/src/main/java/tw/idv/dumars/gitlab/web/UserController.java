package tw.idv.dumars.gitlab.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import tw.idv.dumars.gitlab.domain.User;

@Slf4j
@Controller
public class UserController {

    @Value("${gitlab.api-prefix}")
    private String url;
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public User[] showActiveUsers() {
        return restTemplate.getForObject(String.format("%s/users?active=true", url), User[].class);
    }

    @GetMapping(value = "/projects/{id}/members/all")
    @ResponseBody
    public User[] showAllProjectMembers(@PathVariable int id) {
        return restTemplate.getForObject(String.format("%s/projects/%d/members/all", url, id), User[].class);
    }

    @GetMapping(value = "/groups/{id}/members/all")
    @ResponseBody
    public User[] showAllGroupMembers(@PathVariable int id) {
        return restTemplate.getForObject(String.format("%s/groups/%d/members/all", url, id), User[].class);
    }
}
