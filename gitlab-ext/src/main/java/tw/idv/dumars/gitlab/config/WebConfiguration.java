package tw.idv.dumars.gitlab.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;
import tw.idv.dumars.gitlab.interceptor.OAuth2Interceptor;

import java.util.Collections;

@Configuration
public class WebConfiguration {

	@Bean
	@Primary
	ObjectMapper objectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.findAndRegisterModules();
		return mapper;
	}

	@Bean
	RestTemplate restTemplate() {
		RestTemplate template = new RestTemplate();
		template.setInterceptors(Collections.singletonList(new OAuth2Interceptor()));
		return template;
	}
}
