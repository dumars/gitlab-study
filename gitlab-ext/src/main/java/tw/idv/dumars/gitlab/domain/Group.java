package tw.idv.dumars.gitlab.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Group {

	private Integer id;
	private String name;
	private String path;
	private String description;
	private String visibility;
	@JsonProperty("lfs_enabled")
	private boolean lfsEnabled;
	@JsonProperty("avatar_url")
	private String avatarUrl;
	@JsonProperty("web_url")
	private String webUrl;
	@JsonProperty("request_access_enabled")
	private boolean requestAccessEnabled;

	@JsonProperty("full_name")
	private String fullName;
	@JsonProperty("full_path")
	private String fullPath;
	@JsonProperty("file_template_project_id")
	private Integer fileTemplateProjectId;
	@JsonProperty("parent_id")
	private String parentId;
}
