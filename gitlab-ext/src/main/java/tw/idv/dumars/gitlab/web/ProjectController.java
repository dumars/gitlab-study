package tw.idv.dumars.gitlab.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;
import tw.idv.dumars.gitlab.domain.Project;

import java.security.Principal;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_HTML_VALUE;

@Slf4j
@Controller
public class ProjectController {

	@Value("${gitlab.api-prefix}")
	private String url;
	@Autowired
	private RestTemplate restTemplate;

	@GetMapping(value = "/projects", produces = TEXT_HTML_VALUE)
	public String listProjects(String state, Model model) {
		Project[] projects = restTemplate.getForObject(String.format("%s/projects", url), Project[].class);
		model.addAttribute("projects", projects);
		model.addAttribute("state", state);
		return "project/projects";
	}

	@GetMapping(value = "/projects", produces = APPLICATION_JSON_VALUE)
	public Project[] findMyProjects(Principal principal) {
		StringBuilder builder = new StringBuilder();
		builder.append(url)
				.append("/users/").append(principal.getName())
				.append("/projects?order_by=id");
		return restTemplate.getForObject(builder.toString(), Project[].class);
	}
}
