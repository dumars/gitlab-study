package tw.idv.dumars.gitlab.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Commit {

	@JsonProperty("id")
	private String id;
	@JsonProperty("short_id")
	private String shortId;
	@JsonProperty("title")
	private String title;
	@JsonProperty("message")
	private String message;
	@JsonProperty("author_email")
	private String authorEmail;
	@JsonProperty("author_name")
	private String authorName;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
	@JsonProperty("authored_date")
	private LocalDateTime authoredDate;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
	@JsonProperty("committed_date")
	private LocalDateTime committedDate;
	@JsonProperty("committer_email")
	private String committerEmail;
	@JsonProperty("committer_name")
	private String committerName;
	@JsonProperty("parent_ids")
	private String[] parentIds;
}
