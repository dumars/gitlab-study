package tw.idv.dumars.gitlab.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Branch {

	private String name;
	private boolean merged;
	@JsonProperty("protected")
	private boolean protectedBranch;
	@JsonProperty("default")
	private boolean defaultBranch;
	@JsonProperty("developers_can_push")
	private boolean developersCanPush;
	@JsonProperty("developers_can_merge")
	private boolean developersCanMerge;
	@JsonProperty("can_push")
	private boolean canPush;
	private Commit commit;
}
