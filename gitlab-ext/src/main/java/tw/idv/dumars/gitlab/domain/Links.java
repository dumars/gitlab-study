package tw.idv.dumars.gitlab.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Links {

    private String self;
    private String notes;
    @JsonProperty("award_emoji")
    private String awardEmoji;
    private String project;
}
