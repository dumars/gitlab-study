package tw.idv.dumars.gitlab.domain.enumeration;

public enum IssueScope {
    created_by_me, assigned_to_me, all;
}
