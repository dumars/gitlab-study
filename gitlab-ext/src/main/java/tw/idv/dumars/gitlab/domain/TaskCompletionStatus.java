package tw.idv.dumars.gitlab.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TaskCompletionStatus {

    private int count;
    @JsonProperty("completed_count")
    private int completedCount;
}
