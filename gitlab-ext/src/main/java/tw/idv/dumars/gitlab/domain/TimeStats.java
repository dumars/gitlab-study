package tw.idv.dumars.gitlab.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TimeStats {

    @JsonProperty("time_estimate")
    private Long timeEstimate;
    @JsonProperty("total_time_spent")
    private Long totalTimeSpent;
    @JsonProperty("human_time_estimate")
    private Long humanTimeEstimate;
    @JsonProperty("human_total_time_spent")
    private Long humanTotalTimeSpent;
}
