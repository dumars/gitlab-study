package tw.idv.dumars.gitlab.domain.enumeration;

public enum IssueState {
    opened, closed, reopen, close;
}
