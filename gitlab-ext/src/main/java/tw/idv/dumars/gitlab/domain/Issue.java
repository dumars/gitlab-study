package tw.idv.dumars.gitlab.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Issue {

    private Integer id;
    @JsonProperty("project_id")
    private Integer projectId;
    @JsonProperty("created_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private LocalDateTime createdAt;
    private Integer iid;
    private String title;
    private String state;
    @JsonProperty("assignee_ids")
    private Integer[] assigneeIds;
    private User[] assignees;
    private User assignee;
    private String[] labels;
    @JsonProperty("upvotes")
    private Integer upVotes;
    @JsonProperty("downvotes")
    private Integer downVotes;
    @JsonProperty("merge_requests_count")
    private int mergeRequestsCount;
    @JsonProperty("author")
    private User author;
    private String description;
    @JsonProperty("updated_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private LocalDateTime updatedAt;
    @JsonProperty("closed_at")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private LocalDateTime closedAt;
    @JsonProperty("closed_by")
    private User closedBy;
    private Milestone milestone;
    private boolean subscribed;
    @JsonProperty("user_notes_count")
    private Integer userNotesCount;
    @JsonProperty("due_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate dueDate;
    @JsonProperty("web_url")
    private String webUrl;
    @JsonProperty("time_stats")
    private TimeStats timeStats;
    private boolean confidential;
    @JsonProperty("discussion_locked")
    private boolean discussionLocked;
    @JsonProperty("_links")
    private Links links;
    @JsonProperty("task_completion_status")
    private TaskCompletionStatus taskCompletionStatus;
}
