# gitlab ext

透過 gitlab api 串接來擴展客製功能

```sh
mvn spring-boot:run
```

## prometheus

在 maven 中加入了 prometheus 套件，配合 spring-boot-actuator 套件可以在 [http://localhost:9999/actuator/prometheus](http://localhost:9999/actuator/prometheus) 看到相關資訊

```text
# HELP process_start_time_seconds Start time of the process since unix epoch.
# TYPE process_start_time_seconds gauge
process_start_time_seconds{application="gitlab-ext",} 1.568276498961E9
# HELP jvm_classes_unloaded_classes_total The total number of classes unloaded since the Java virtual machine has started execution
# TYPE jvm_classes_unloaded_classes_total counter
jvm_classes_unloaded_classes_total{application="gitlab-ext",} 0.0
# HELP tomcat_sessions_active_current_sessions  
# TYPE tomcat_sessions_active_current_sessions gauge
tomcat_sessions_active_current_sessions{application="gitlab-ext",} 0.0
# HELP system_cpu_count The number of processors available to the Java virtual machine
# TYPE system_cpu_count gauge
system_cpu_count{application="gitlab-ext",} 12.0
# HELP jvm_threads_states_threads The current number of threads having NEW state
# TYPE jvm_threads_states_threads gauge
jvm_threads_states_threads{application="gitlab-ext",state="blocked",} 0.0
jvm_threads_states_threads{application="gitlab-ext",state="waiting",} 11.0
jvm_threads_states_threads{application="gitlab-ext",state="new",} 0.0
jvm_threads_states_threads{application="gitlab-ext",state="terminated",} 0.0
jvm_threads_states_threads{application="gitlab-ext",state="runnable",} 9.0
jvm_threads_states_threads{application="gitlab-ext",state="timed-waiting",} 4.0
...ignore
```

## GitLab OAuth2

1. 建立 Application: 使用管理者身分登入， Applications -> New Application

    - Name: gitlab-ext

    - Redirect URI: `http://localhost:9999/login`
  
    - 下面全部勾選(做測試就先全部勾選吧，避免缺東缺西的)
  
    - Submit 之後會有 Application ID 及 Secret，在下一步要用到

2. 設定 application.yml，更換以下的值(因為每次新增時的值都是不同的，所以如果有重建或重裝都要修改)

    - security.oauth2.client.client-id 對應 Application ID 的值
    
    - security.oauth2.client.client-secret 對應 Secret 的值

3. 重新啟動 spring-boot，輸入 [http://localhost:9999/main](http://localhost:9999/main) 會被轉址到 gitlab 詢問同意或拒絕，同意之後就會轉回來，即可看到你的帳號名稱。