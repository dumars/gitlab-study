# GitLab Study

根目錄下有兩個資料夾，簡單說明：

1. docker: gitlab-ce docker container 的操作說明，並且調整原始設定，開放一些內建的套件讓我們外部程式使用。

2. gitlab-ext: java 程式，主要是要串接 gitlab api 來開發額外的功能。

以上兩個資料夾內也有 README，請參照說明操作。